level = {}

-- The maximum size of a level.
level.level_size = {x = 16, y = 16, z = 16}

-- The level name which is active at the moment.
level.current = "level1"

-- The path in which the levels are stored.
level.level_path = minetest.get_worldpath() .. "/schems/"

-- The path in which exported levels are stored.
level.custom_level_path = minetest.get_worldpath() .. "/schems/"

-- Function called before a level gets loaded. Override this as you wish.
function level.before_load(name)

end

-- Function called after a level gets loaded. Override this as you wish.
function level.after_load(name)

end

-- Function called to save game data. Override this as you wish.
function level.save_game(file)
	
end

-- The format to use to export levels. "mts" or "we"
level.export_format = "mts"

-- A function called before the level is exported. It should remove all
-- technical nodes and return the annotation of the level.
-- Override this as you wish.
function level.before_export(pos1, pos2, name)
	return ""
end

-- A function called after the level is exported. It should revert the
-- operations done in level.before_export. Override this as you wish.
function level.after_export(pos1, pos2, name)

end

-- Function called before a level is imported in creative. Override this as you wish.
function level.before_import(name)

end

-- Function called after a level is imported in creative. Override this as you wish.
function level.after_import(name)

end

-- This function loads a level which is stored in <name>.we or <name>.mts and <name>.lua.
-- This function is not designed to be overriden.
function level.load_level(name, creative)
	local path
	if creative then
		path = level.custom_level_path .. name
	else
		path = level.level_path .. name
	end
	local file, err = io.open(path .. ".lua", "rb")
	if err then
		print("level not found: " .. name)
		return
	end
	file:close()
	
	if creative then
		level.before_import(name)
	else
		level.before_load(name)
		level.current = name
	end

	minetest.delete_area({x = 0, y = 0, z = 0}, level.level_size)
	local file, err = io.open(path .. ".we", "rb")
	if err then
		local file, err = io.open(path .. ".mts", "rb")
		if err then
			print("unsupported level format: " .. name)
			return
		end
		file:close()
		minetest.place_schematic({x = 0, y = 0, z = 0}, path..".mts")
	else
		local value = file:read("*a")
		file:close()
		worldedit.deserialize({x = 0, y = 0, z = 0}, value)
	end
	dofile(path .. ".lua")

	if not creative then
		file, err = io.open(minetest.get_worldpath() .. "/level.txt", "wb")
		if err then
			print("Failed to save into level.txt")
		else
			file:write("level.current = \""..level.current.."\"\n")
			file:flush()
			level.save_game(file)
			file:close()
		end
	end
	
	if creative then
		level.after_import(name)
	else
		level.after_load(name)
	end
end

-- Returns true if the level exists
function level.level_exists(name)
	local path = level.level_path .. name
	local file, err = io.open(path .. ".lua", "rb")
	if err then
		return false
	else
		file:close()
		return true
	end
end

-- Reload the current level. No need to override this.
function level.restart_level()
	level.load_level(level.current)
end

-- Do not allow multiplayer
minetest.register_on_prejoinplayer(function(name, ip)
	if name ~= "singleplayer" then
		return "This game is not designed for multiplayer, sorry."
	end
end)

-- Load the last level on startup
minetest.register_on_joinplayer(function(player)
	local fn = minetest.get_worldpath() .. "/level.txt"
	local file, err = io.open(fn)
	if err then
		print("No saved game found")
	else
		file:close()
		dofile(fn)
	end
	
	level.load_level(level.current)
end)

minetest.register_tool("level:save", {
	description = "Save Level",
	inventory_image = "level_save.png",
	stack_max = 1,
	range = 0,
	on_use = function(itemstack, placer, pointed_thing)
		local name = placer:get_player_name()
		local pos1 = worldedit.pos1[name]
		local pos2 = worldedit.pos2[name]
		if pos1 == nil or pos2 == nil then
			minetest.chat_send_player(name, "Use the worldedit wand to mark the level.")
			return itemstack
		end
		local gui = "size[3,1.1]" ..
		"field[0.3,0.6;3.0,1.0;file;Save level as ...;]"
		minetest.show_formspec(name, "level.save", gui)
		return itemstack
	end,
})

minetest.register_tool("level:open", {
	description = "Open Level",
	inventory_image = "level_open.png",
	stack_max = 1,
	range = 0,
	on_use = function(itemstack, placer, pointed_thing)
		local name = placer:get_player_name()
		local gui = "size[3,1.1]" ..
		"field[0.3,0.6;3.0,1.0;file;Open level ...;]"
		minetest.show_formspec(name, "level.open", gui)
		return itemstack
	end,
})

minetest.register_on_player_receive_fields(function(player, form, pressed)
	if form == "level.save" then
		local file = pressed.file
		local name = player:get_player_name()
		minetest.close_formspec(name, form)
		if file == nil then
			return
		end
		file = level.custom_level_path .. file
		
		local pos1 = worldedit.pos1[name]
		local pos2 = worldedit.pos2[name]
		if pos1 == nil or pos2 == nil then
			minetest.chat_send_player(name, "Use the worldedit wand to mark the level.")
			return itemstack
		end
		local x1 = math.min(pos1.x, pos2.x)
		local x2 = math.max(pos1.x, pos2.x)
		local y1 = math.min(pos1.y, pos2.y)
		local y2 = math.max(pos1.y, pos2.y)
		local z1 = math.min(pos1.z, pos2.z)
		local z2 = math.max(pos1.z, pos2.z)
		pos1 = {x=x1, y=y1, z=z1}
		pos2 = {x=x2, y=y2, z=z2}
		
		minetest.mkdir(level.custom_level_path)
		data = level.before_export(pos1, pos2, file)
		if level.export_format == "mts" then
			minetest.create_schematic(pos1, pos2, nil, file..".mts")
		elseif level.export_format == "we" then
			local f, err = io.open(file..".we", "wb")
			if err ~= nil then
				minetest.chat_send_player(name, "Could not save file to \"" .. file .. ".we\"")
				return
			end
			local result, count = worldedit.serialize(pos1, pos2)
			f:write(result)
			f:flush()
			f:close()
		end
		
		local f, err = io.open(file..".lua", "wb")
		if err ~= nil then
			minetest.chat_send_player(name, "Could not save file to \"" .. file .. ".lua\"")
			return
		end
		f:write(data)
		f:flush()
		f:close()
		level.after_export(pos1, pos2, file)
		minetest.chat_send_player(name, "Level saved to \"" .. file .. "\"")
		return
	elseif form == "level.open" then
		local file = pressed.file
		local name = player:get_player_name()
		minetest.close_formspec(name, form)
		if file == nil then
			return
		end
		
		level.load_level(file, true)
		minetest.chat_send_player(name, "Opened level \"" .. level.custom_level_path .. file .. "\"")
	end
end)

